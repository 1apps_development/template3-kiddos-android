package com.workdo.kiddos.adapter

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.kiddos.R
import com.workdo.kiddos.api.ApiClient
import com.workdo.kiddos.databinding.CellSearchProductBinding
import com.workdo.kiddos.model.CategoriesModel
import com.workdo.kiddos.model.FeaturedProductsSub
import com.workdo.kiddos.ui.activity.ActProductDetails
import com.workdo.kiddos.utils.Constants
import com.workdo.kiddos.utils.ExtensionFunctions.hide
import com.workdo.kiddos.utils.ExtensionFunctions.show
import com.workdo.kiddos.utils.SharePreference
import com.workdo.kiddos.utils.Utils

class SearchListAdapter(
    private val context: Activity,
    private val providerList: ArrayList<FeaturedProductsSub>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<SearchListAdapter.ViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency_name).toString()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = CellSearchProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(providerList[position], context, position, itemClick)

    }

    override fun getItemCount(): Int {
        return providerList.size
    }

    inner   class ViewHolder(private val binding: CellSearchProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            Log.e("Cuurent", currency.toString())
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath)).into(binding.ivProduct)
            binding.tvProductDesc.text = data.name.toString()
            binding.tvProductPrice.text = Utils.getPrice(data.finalPrice.toString())
            binding.tvcurrenytype.text = currency
            binding.tvProduct.text = data.tagApi?.uppercase()

            if (data.inWhishlist == true) {
                binding.ivWishlist.background = androidx.core.content.res.ResourcesCompat.getDrawable(context.resources, R.drawable.ic_like, null)
            } else {
                binding.ivWishlist.background = androidx.core.content.res.ResourcesCompat.getDrawable(context.resources, R.drawable.ic_dislike, null)
            }

            if(Utils.isLogin(context))
            {
                binding.ivWishlist.show()
            }else
            {
                binding.ivWishlist.hide()

            }
            binding.ivWishlist.setOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            binding.btnAddtoCart.setOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }


        }
    }


}