package com.workdo.kiddos.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.kiddos.R
import com.workdo.kiddos.model.CategoriesModel

class AllProductCategoresAdapter(
    var context: Activity,
    private val mList: List<CategoriesModel>,
    private val onFilterClick: (String, String) -> Unit
) : RecyclerView.Adapter<AllProductCategoresAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_productlisting, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val categoriesModel = mList[position]
        if (categoriesModel.id==0){
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.border_appcolor_15, null)
            holder.tvProductName.setTextColor(
                AppCompatResources.getColorStateList(
                    context, R.color.black
                )
            )
        }else{
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.border_white_15, null)
            holder.tvProductName.setTextColor(
                AppCompatResources.getColorStateList(
                    context, R.color.black
                )
            )
        }
        holder.tvProductName.text=categoriesModel.cateName
        Glide.with(context)
            .load(categoriesModel.image).into(holder.iv_cart)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvProductName: TextView = itemView.findViewById(R.id.tvProductName)
        val card: ConstraintLayout = itemView.findViewById(R.id.card)
        val iv_cart: ImageView = itemView.findViewById(R.id.iv_cart)
    }

    private fun onFilterClick(id: String, name: String) {
        onFilterClick.invoke(id, name)
    }
}