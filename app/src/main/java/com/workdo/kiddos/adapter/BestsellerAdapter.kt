package com.workdo.kiddos.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.kiddos.R
import com.workdo.kiddos.api.ApiClient
import com.workdo.kiddos.databinding.CellHomeproductsBinding
import com.workdo.kiddos.model.FeaturedProductsSub
import com.workdo.kiddos.utils.Constants
import com.workdo.kiddos.utils.ExtensionFunctions.hide
import com.workdo.kiddos.utils.ExtensionFunctions.show
import com.workdo.kiddos.utils.SharePreference
import com.workdo.kiddos.utils.Utils

class BestsellerAdapter(
    private val context: Activity,
    private val providerList: ArrayList<FeaturedProductsSub>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<BestsellerAdapter.BestSellerViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency).toString()

    inner class BestSellerViewHolder(private val binding: CellHomeproductsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            Log.e("Cuurent", currency)
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath))
                .into(binding.ivProduct)
            binding.tvProductDesc.text = data.name.toString()
            binding.tvProductPrice.text =
                Utils.getPrice(data.finalPrice.toString())
            binding.tvcurrenytype.text = currency
            binding.tvProduct.text = data.tagApi?.uppercase() ?: data.tagApi

            if (data.inWhishlist == true) {
                binding.ivWishlist.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_like, null)
            } else {
                binding.ivWishlist.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_dislike, null)
            }

            if(Utils.isLogin(context))
            {
                binding.ivWishlist.show()
            }else
            {
                binding.ivWishlist.hide()

            }
            binding.ivWishlist.setOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            binding.btnAddtoCart.setOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BestSellerViewHolder {
        val view =
            CellHomeproductsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return BestSellerViewHolder(view)
    }

    override fun onBindViewHolder(holder: BestSellerViewHolder, position: Int) {
        holder.bind(providerList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return providerList.size
    }
}