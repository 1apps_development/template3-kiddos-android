package com.workdo.kiddos.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.kiddos.R
import com.workdo.kiddos.model.CategoriesModel
import com.workdo.kiddos.ui.activity.ActProductDetails

class WishListAdapter(
    var context: Activity,
    private val mList: List<CategoriesModel>,
    private val onFilterClick: (String, String) -> Unit
) : RecyclerView.Adapter<WishListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_orderdetailslist, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val categoriesModel = mList[position]
        holder.tvCardProductName.text = categoriesModel.cateName
        Glide.with(context)
            .load(categoriesModel.image).into(holder.iv_cart)
        holder.itemView.setOnClickListener {
            val intent = Intent(context, ActProductDetails::class.java)
            context.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvCardProductName: TextView = itemView.findViewById(R.id.tvCardProductName)
        val iv_cart: ImageView = itemView.findViewById(R.id.iv_cart)
    }

    private fun onFilterClick(id: String, name: String) {
        onFilterClick.invoke(id, name)
    }
}