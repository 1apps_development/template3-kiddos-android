package com.workdo.kiddos.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.workdo.kiddos.R
import com.workdo.kiddos.databinding.CellOrderHistoryBinding
import com.workdo.kiddos.model.CategoriesModel
import com.workdo.kiddos.model.OrderListData
import com.workdo.kiddos.ui.activity.ActOrderDetails
import com.workdo.kiddos.utils.Constants
import com.workdo.kiddos.utils.SharePreference
import com.workdo.kiddos.utils.Utils

class OrderHistoryAdapter(
    private val context: Activity,
    private val orderlist: ArrayList<OrderListData>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<OrderHistoryAdapter.WishlistViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency)

    inner class WishlistViewHolder(private val binding: CellOrderHistoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: OrderListData,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            binding.tvDate.text =
                context.getString(R.string.date).plus(" ").plus(Utils.getDate(data.date.toString()))
            binding.tvOrderId.text = "#".plus(data.productOrderId)
            binding.tvFeaturedProductPrice.text =
                currency.plus(Utils.getPrice(data.amount.toString()))
            binding.tvOrderStatus.text = context.getString(R.string.statue_).plus(" ").plus(data.deliveredStatusString)
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WishlistViewHolder {
        val view =
            CellOrderHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WishlistViewHolder(view)
    }

    override fun onBindViewHolder(holder: WishlistViewHolder, position: Int) {
        holder.bind(orderlist[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return orderlist.size
    }
}