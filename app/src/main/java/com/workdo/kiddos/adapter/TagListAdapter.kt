package com.workdo.kiddos.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.kiddos.R
import com.workdo.kiddos.model.CategorylistData
import com.workdo.kiddos.utils.ItemClick

class TagListAdapter(
    var context: Activity,
    private val mList: ArrayList<CategorylistData>,
    val callBack: ItemClick
) :
    RecyclerView.Adapter<TagListAdapter.ViewHolder>() {
    var currency: String = ""
    var currencyPosition: String = ""
    var price = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_tag, parent, false)

        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemSubAddons = mList[position]

        holder.tagName.text = itemSubAddons.name
        if (!itemSubAddons.isSelect) {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.border_green_10, null)
            holder.tagName.setTextColor(ContextCompat.getColor(context, R.color.green))
        } else {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.green_10, null)
            holder.tagName.setTextColor(ContextCompat.getColor(context, R.color.white))
        }

        holder.itemView.setOnClickListener {
            if (!itemSubAddons.isSelect) {
                holder.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.green_10, null)
               holder.tagName.setTextColor(ContextCompat.getColor(context, R.color.white))

                mList[position].isSelect = true
            } else {
                holder.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.border_green_10, null)
                holder.tagName.setTextColor(ContextCompat.getColor(context, R.color.green))
                mList[position].isSelect = false
            }
            callBack.onClick("Click",getName(),getId())
        }
    }

    private fun getName():  ArrayList<String> {
        val list = ArrayList<String>()

        for (i in 0 until mList.size) {
            if (mList[i].isSelect) {
                list.add( mList[i].name.toString())
            }
        }
        return list
    }

    private fun getId():  ArrayList<String> {
        val list = ArrayList<String>()

        for (i in 0 until mList.size) {
            if (mList[i].isSelect) {
                list.add( mList[i].id.toString())
            }
        }
        return list
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tagName: TextView = itemView.findViewById(R.id.tagName)
        val card: ConstraintLayout = itemView.findViewById(R.id.card)
    }

}