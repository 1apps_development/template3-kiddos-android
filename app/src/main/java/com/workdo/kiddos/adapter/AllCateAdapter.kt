package com.workdo.kiddos.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.kiddos.api.ApiClient
import com.workdo.kiddos.databinding.CellHomecategoriesBinding
import com.workdo.kiddos.model.HomeCategoriesItem
import com.workdo.kiddos.ui.activity.ActCategoryProduct
import com.workdo.kiddos.ui.activity.ActMenuCatProduct
import com.workdo.kiddos.utils.Constants

class AllCateAdapter(
    private val context: Activity,
    private val categoryList: ArrayList<HomeCategoriesItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<AllCateAdapter.AllCateViewHolder>() {

    inner class AllCateViewHolder(private val binding: CellHomecategoriesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: HomeCategoriesItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {

            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.image))
                .into(binding.ivCategories)
            binding.tvCategoriesName.text = data.name.toString()

           /* itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }*/


            itemView.setOnClickListener {
                val intent = Intent(itemView.context, ActCategoryProduct::class.java)
                intent.putExtra("maincategory_id", data.id.toString())
                intent.putExtra("name", data.name.toString())

                itemView.context.startActivity(intent)
            }

            binding.btnGotoCategories.setOnClickListener {
                val intent = Intent(itemView.context, ActCategoryProduct::class.java)
                intent.putExtra("maincategory_id", data.id.toString())
                intent.putExtra("name", data.name.toString())

                itemView.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllCateViewHolder {
        val view =
            CellHomecategoriesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AllCateViewHolder(view)
    }

    override fun onBindViewHolder(holder: AllCateViewHolder, position: Int) {
        holder.bind(categoryList[position], context, position, itemClick)


    }

    override fun getItemCount(): Int {

        return categoryList.size
    }
}