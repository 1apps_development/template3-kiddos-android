package com.workdo.kiddos.utils;

import com.workdo.kiddos.model.CountryDataItem;

public interface OnItemClickListenerGuestCountry {
    void onItemClickGuest(CountryDataItem item);
}
