package com.workdo.kiddos.utils;

import com.workdo.kiddos.model.CountryDataItem;

public interface OnItemClickListenerCountry {
    void onItemClick(CountryDataItem item);
}
