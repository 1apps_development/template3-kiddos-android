package com.workdo.kiddos.utils;

import com.workdo.kiddos.model.StateListData;

public interface OnItemClickListenerState {
    void onItemClickState(StateListData item);
}
