package com.workdo.kiddos.utils;

import com.workdo.kiddos.model.CityListData;

public interface OnItemClickListenerCity {
    void onItemClickCity(CityListData item);
}
