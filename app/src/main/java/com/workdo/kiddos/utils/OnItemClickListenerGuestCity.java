package com.workdo.kiddos.utils;

import com.workdo.kiddos.model.CityListData;

public interface OnItemClickListenerGuestCity {
    void onItemClickCityGuest(CityListData item);
}
