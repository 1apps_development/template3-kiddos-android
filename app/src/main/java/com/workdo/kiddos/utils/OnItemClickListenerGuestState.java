package com.workdo.kiddos.utils;

import com.workdo.kiddos.model.StateListData;

public interface OnItemClickListenerGuestState {
    void onItemClickStateGuest(StateListData item);
}
