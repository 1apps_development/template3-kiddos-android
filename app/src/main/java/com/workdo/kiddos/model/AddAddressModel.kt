package com.workdo.kiddos.model

import com.google.gson.annotations.SerializedName

data class AddAddressModel(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

