package com.workdo.kiddos.model

import com.google.gson.annotations.SerializedName
import com.workdo.kiddos.model.Data

data class FargotPasswordSendOtpModel(

    @field:SerializedName("data")
	val data: Data? = null,

    @field:SerializedName("message")
	val message: String? = null,

    @field:SerializedName("status")
	val status: Int? = null
)

