package com.workdo.kiddos.model

import com.google.gson.annotations.SerializedName

data class EditProfileUpdateModel(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class EditData(

	@field:SerializedName("message")
	val message: String? = null
)
