package com.workdo.kiddos.ui.authentication

import android.view.View
import com.workdo.kiddos.base.BaseActivity
import com.workdo.kiddos.databinding.ActSuccessfullBinding
import com.workdo.kiddos.ui.activity.MainActivity

class ActSuccessfull : BaseActivity() {
    private lateinit var _binding: ActSuccessfullBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActSuccessfullBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnGotoyourstore.setOnClickListener { openActivity(MainActivity::class.java) }
    }
}
