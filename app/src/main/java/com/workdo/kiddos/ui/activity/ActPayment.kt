package com.workdo.kiddos.ui.activity

import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.kiddos.R
import com.workdo.kiddos.adapter.PaymentAdapter
import com.workdo.kiddos.api.ApiClient
import com.workdo.kiddos.base.BaseActivity
import com.workdo.kiddos.databinding.ActPaymentBinding
import com.workdo.kiddos.model.CategoriesModel
import com.workdo.kiddos.model.PaymentData
import com.workdo.kiddos.remote.NetworkResponse
import com.workdo.kiddos.ui.authentication.ActWelCome
import com.workdo.kiddos.ui.option.ActCart
import com.workdo.kiddos.utils.ExtensionFunctions.hide
import com.workdo.kiddos.utils.ExtensionFunctions.show
import com.workdo.kiddos.utils.SharePreference
import com.workdo.kiddos.utils.Utils
import kotlinx.coroutines.launch

class ActPayment : BaseActivity() {
    private lateinit var _binding: ActPaymentBinding
    private var paymentList = ArrayList<PaymentData>()
    private lateinit var paymentAdapter: PaymentAdapter
    private var manager: GridLayoutManager? = null
    var comment = ""
    var paymentName = ""
    var stripeKey = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActPaymentBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        Log.e("stripeKey", stripeKey)

        _binding.ivBack.setOnClickListener { finish() }
        _binding.btnContinue.setOnClickListener {

            if (_binding.chbTermsCondition.isChecked){

                comment =_binding.edNote.text.toString()
                SharePreference.setStringPref(this@ActPayment,SharePreference.Payment_Comment,comment.toString())
                openActivity(ActConfirm::class.java)
            }
        }

        _binding.clcart.setOnClickListener { openActivity(ActCart::class.java) }
        _binding.chbTermsCondition.setOnClickListener {
            _binding.btnContinue.isEnabled = _binding.chbTermsCondition.isChecked
        }

        manager = GridLayoutManager(this@ActPayment, 1, GridLayoutManager.VERTICAL, false)
        _binding.tvTerms.setOnClickListener {
            val terms=SharePreference.getStringPref(this@ActPayment,SharePreference.Terms).toString()
            val uri:Uri = Uri.parse(terms)

            val intent =Intent(Intent.ACTION_VIEW,uri)
            startActivity(intent)
        }

    }

    private fun callPaymentList() {
        Utils.showLoadingProgress(this@ActPayment)
        val paymenthashmap = HashMap<String,String>()
        paymenthashmap["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActPayment)
                .paymentList(paymenthashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val paymentListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.size ?: 0) > 0) {
                                _binding.rvPayment.show()
                                _binding.vieww.hide()

                                runOnUiThread {
                                    paymentListResponse.data?.let {
                                        paymentList.addAll(it)
                                    }
                                }
                                paymentList.removeAll {
                                    it.status == "off"
                                }
                                (paymentList)

                            } else {
                                _binding.rvPayment.hide()
                                _binding.vieww.show()
                            }
                            paymentAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActPayment,
                                paymentListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActPayment,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActPayment)
                    } else {
                        Utils.errorAlert(
                            this@ActPayment,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActPayment,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActPayment,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun PaymentListAdapter(paymentList: ArrayList<PaymentData>) {
        _binding.rvPayment.layoutManager = manager
        paymentAdapter =
            PaymentAdapter(this@ActPayment, paymentList) { i: Int, s: String ->
                if (paymentList[i].isSelect == true) {
                    paymentName = paymentList[i].nameString.toString()

                    SharePreference.setStringPref(
                        this@ActPayment,
                        SharePreference.Payment_Type,
                        paymentName
                    )

                    SharePreference.setStringPref(
                        this@ActPayment,
                        SharePreference.PaymentImage,
                        paymentList[i].image.toString()
                    )
                } else {

                }
            }
        _binding.rvPayment.adapter = paymentAdapter
    }

    override fun onResume() {
        super.onResume()
        paymentList.clear()
        PaymentListAdapter(paymentList)
        callPaymentList()
    }

}