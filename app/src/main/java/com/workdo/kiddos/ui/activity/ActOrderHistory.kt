package com.workdo.kiddos.ui.activity

import android.content.Intent
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.kiddos.R
import com.workdo.kiddos.adapter.OrderHistoryAdapter
import com.workdo.kiddos.api.ApiClient
import com.workdo.kiddos.base.BaseActivity
import com.workdo.kiddos.databinding.ActOrderHistoryBinding
import com.workdo.kiddos.model.CategoriesModel
import com.workdo.kiddos.model.OrderListData
import com.workdo.kiddos.remote.NetworkResponse
import com.workdo.kiddos.ui.authentication.ActWelCome
import com.workdo.kiddos.ui.option.ActCart
import com.workdo.kiddos.utils.Constants
import com.workdo.kiddos.utils.ExtensionFunctions.hide
import com.workdo.kiddos.utils.ExtensionFunctions.show
import com.workdo.kiddos.utils.PaginationScrollListener
import com.workdo.kiddos.utils.SharePreference
import com.workdo.kiddos.utils.Utils
import kotlinx.coroutines.launch

class ActOrderHistory : BaseActivity() {
    private lateinit var _binding: ActOrderHistoryBinding
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    private var orderList = ArrayList<OrderListData>()
    private lateinit var orderlistAdapter: OrderHistoryAdapter
    private var manager: LinearLayoutManager? = null


    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActOrderHistoryBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }

        manager = LinearLayoutManager(this@ActOrderHistory)
        orderListAdapter(orderList)
        pagination()

    }

    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callOrderList()
            }

        }
        _binding.rvOrderHistory.addOnScrollListener(paginationListener)
    }

    private fun callOrderList() {
        Utils.showLoadingProgress(this@ActOrderHistory)
        val categoriesProduct =HashMap<String,String>()
        categoriesProduct["user_id"]=SharePreference.getStringPref(this@ActOrderHistory,SharePreference.userId).toString()
        categoriesProduct["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActOrderHistory)
                .getOrderList(currentPage.toString(), categoriesProduct)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val OrderListResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvOrderHistory.show()
                                _binding.tvNoDataFound.hide()
                                currentPage =
                                    OrderListResponse?.currentPage!!.toInt()
                                total_pages =
                                    OrderListResponse.lastPage!!.toInt()
                                OrderListResponse.data?.let {
                                    orderList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvOrderHistory.hide()
                                _binding.tvNoDataFound.show()
                            }
                            orderlistAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActOrderHistory,
                                OrderListResponse?.data?.get(0)?.message.toString())
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActOrderHistory,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActOrderHistory)
                    }else{
                        Utils.errorAlert(
                            this@ActOrderHistory,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActOrderHistory,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActOrderHistory,
                        "Something went wrong"
                    )
                }
            }
        }

    }


    private fun orderListAdapter(orderList: ArrayList<OrderListData>) {
        _binding.rvOrderHistory.layoutManager=manager
        orderlistAdapter=OrderHistoryAdapter(this@ActOrderHistory,orderList){ i:Int,s:String ->
            if (s == Constants.ItemClick){
                startActivity(Intent(this@ActOrderHistory,ActOrderDetails::class.java).putExtra("order_ID",orderList[i].id.toString()))
            }

        }
        _binding.rvOrderHistory.adapter=orderlistAdapter
    }

    override fun onResume() {
        super.onResume()
        currentPage =1
        isLoading=false
        isLastPage=false
        orderList.clear()
        callOrderList()
    }

}