package com.workdo.kiddos.ui.option

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.kiddos.R
import com.workdo.kiddos.adapter.CartListAdapter
import com.workdo.kiddos.base.BaseActivity
import com.workdo.kiddos.databinding.ActCartBinding
import com.workdo.kiddos.model.CategoriesModel
import com.workdo.kiddos.ui.activity.ActShoppingCart

class ActCart : BaseActivity() {
    private lateinit var _binding: ActCartBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActCartBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }
        _binding.btnAllProduct.setOnClickListener { openActivity(ActShoppingCart::class.java) }


    }
}